SOURCE ?= cv.tex cv_pres.tex
BACKEND ?= pdflua
TARGET = $(patsubst %.tex,%.pdf,$(SOURCE))

TEXFLAGS ?= -file-line-error -halt-on-error

# Makefile options
.DEFAULT_GOAL := all
.NOTPARALLEL:

.PHONY: all
all: $(TARGET)

%.pdf: %.tex
	latexmk -$(BACKEND) $(TEXFLAGS) $^

.PHONY: clean
clean: cleanextra
	$(foreach s,$(SOURCE),latexmk -c $(s);)

.PHONY: distclean
distclean: cleanextra
	$(foreach s,$(SOURCE),latexmk -C $(s);)

.PHONY: cleanextra
cleanextra:
	find . -maxdepth 1 -type f \( \
	-iname "*.nav" \
	-o -iname "*.snm" \
	\) -delete
