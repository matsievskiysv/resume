% Inbuilt themes in beamer
\documentclass{beamer}

\usepackage{hyperref}
\usepackage{fontawesome5}
\usepackage{graphicx}
\graphicspath{{images}}

\usepackage{polyglossia}
\setdefaultlanguage{english}
\setotherlanguage{russian}

\setmonofont{LiberationMono}[Scale=0.87]
\newfontfamily\cyrillicfonttt{LiberationMono}[Scale=0.87]
\defaultfontfeatures{Ligatures=TeX}
\setmainfont{LiberationSerif}
\newfontfamily\cyrillicfont{LiberationSerif}
\setsansfont{LiberationSans}
\newfontfamily\cyrillicfontsf{LiberationSans}

\makeatletter
\newcommand*{\rom}[1]{\expandafter\@slowromancap\romannumeral #1@}
\makeatother

\usepackage{tikz}
\usetikzlibrary{
	arrows.meta,
	automata, %  is used for drawing "finite state automata and Turing Machines". To draw these graphs, each node, its name and relative position is defined, as well as the types of path between each
	backgrounds, % defines background for pictures
	calc, % make complex coordinate calculations
	calendar, % display calendars
	chains, % align nodes o chains
	decorations, % decorate paths
	decorations.pathmorphing,
	er, % each node is defined, as is each edge between each node, as well as any attributes
	graphs, % draw graphs
	graphdrawing,
	intersections, % calculate intersections of paths
	mindmap,
	matrix, % display matrices
	math,
	folding, % paper folding library
	patterns, % defines patterns for filling areas
	petri, % draw Petri nets
	plotmarks, % additional styles for plots
	shapes, % define shapes other than rectangle, circle and coordinate
	snakes, % curved lines
	trees, % Each point on the tree is defined as a node, with children, and each child can have its own children
}
\usegdlibrary{trees}
\usepackage{pgfplots}
\pgfplotsset{compat=newest}
\usepackage[european]{circuitikz}

\usepackage{xfrac}
\usepackage[locale = DE,
list-separator       ={;\,},
list-final-separator ={;\,},
list-pair-separator  ={;\,},
range-units          = single,
range-phrase         ={\text{\ensuremath{-}}},
% quotient-mode        = fraction, % красивые дроби могут не соответствовать ГОСТ
fraction-function    =\sfrac,
separate-uncertainty,
]{siunitx}


\usepackage{pgfplots}
\usepgfplotslibrary{groupplots}
\pgfplotsset{compat=newest}
\pgfkeys{/pgf/number format/.cd,use comma,1000 sep={}}
\usepgfplotslibrary{units}

% Theme choice:
\usetheme{default}
\setbeamertemplate{footline}[text line]{%
  \parbox{\linewidth}{\vspace*{-8pt}\inserttitle\hfill\insertshortauthor\hfill\insertpagenumber/\inserttotalframenumber}}
\setbeamertemplate{navigation symbols}{}%remove navigation symbols

% Title page details:
\title{Selected projects}
\author{Sergey Matsievskiy}
\date{\today}


\begin{document}

% Title page frame
\begin{frame}
    \titlepage
\end{frame}


% Outline frame
\begin{frame}{Outline}
    \tableofcontents
\end{frame}


% Lists frame
\section{Calculation projects}
\subsection{BeamImpedance2DX}

\begin{frame}{BeamImpedance2DX overview}
        \begin{itemize}
                \item Simulation of Maxwell's fields in 2D structures with monopole and dipole
                source function
                \item Written in Python using FEniCSx library
                \item Distributed computations using PETSc
                \item Repo \href{https://gitlab.com/matsievskiysv/bi2d}{\faGitlab\,/matsievskiysv/bi2d}
        \end{itemize}
\end{frame}

\begin{frame}{Contribution}
        \begin{itemize}
                \item Repeated derivation of the weak formulation using Helmholtz
                irrotational/solenoidal filed split and real/imaginary split
                \item Simplified equations by removing real/imaginary part split
                \item Reduced number of governing equations \(9+32+8 \to 3+9+2\)
                \item 2x computation speed up
                \item Implemented both real and complex versions of the program using FEniCSx
                \item Tested code by comparing solution with other programs
        \end{itemize}
\end{frame}

\begin{frame}{Features}
        \begin{itemize}
                \item Sane API
                \item Lazy evaluation
                \item Best to use with Docker/Jupyter or minislurm job queue (\href{https://gitlab.com/matsievskiysv/minislurm}{\faGitlab\,/matsievskiysv/minislurm})
        \end{itemize}
\end{frame}

\begin{frame}{Governing equations (complex version)}
        Irrotational part
        \[
        \underline{\varepsilon}\beta c_0\int_\Omega{\nabla_\perp v \cdot \nabla_\perp\Phi \;d\Omega}
        +\frac{\omega^2\underline{\varepsilon}}{\beta c_0}\int_\Omega{v\Phi \;d\Omega}
        =\int_\Omega{v J_s \;d\Omega}
        \]

        Solenoidal part
        \[
        \left[
                \begin{pmatrix}
                        S_{\perp\perp} &
                        S_{\perp z} \\
                        S_{z \perp} &
                        S_{zz}
                \end{pmatrix}
                +
                \begin{pmatrix}
                        M_{\perp} & 0 \\
                        0 & M_{z}
                \end{pmatrix}
                +
                \begin{pmatrix}
                        B_{\perp\perp} & 0 \\
                        B_{z\perp} & B_{zz}
                \end{pmatrix}
        \right]
        E_{curl}=R
        \]

        \[\hat{S}_{\perp\perp}
        =\int_\Omega{\left(\hat{\operatorname{B}}\vec{w}\right)\left(\frac{1}{\mu}\hat{\operatorname{B}}\underline{\vec{E}}_\perp\right)\;d\Omega}
        +\frac{\omega^2}{\beta^2 c_0^2}\int_\Omega{\vec{w}\frac{1}{\mu}\underline{\vec{E}}_\perp\;d\Omega}\]

        \[\hat{S}_{\perp z}
                =\int_{\Omega}{\vec{w}\hat{\operatorname{Z}}\frac{1}{\underline{\mu}}\hat{\operatorname{A}}\underline{E}_z\;d\Omega}\]
        Etc. \href{https://gitlab.com/matsievskiysv/bi2d/-/tree/master/doc}{\faGitlab\,/matsievskiysv/bi2d/-/tree/master/doc}
\end{frame}

\begin{frame}{Field visualization}
        \centering{
                \includegraphics[width=\columnwidth]{bi2d_ch1}
                \includegraphics[width=\columnwidth]{bi2d_ch2}
        }
\end{frame}

\subsection{LinacCalc}

\begin{frame}{LinacCalc overview}
        \begin{itemize}
                \item Approximation of distributed fields using lumped elements
                \item Written in R using Shiny Web UI library
                \item All equations were checked using Sympy symbolic calculation library
                \item Repo
                \href{https://gitlab.com/matsievskiysv/linaccalc}{\faGitlab\,/matsievskiysv/linaccalc}
                \item Sympy repo \href{https://gitlab.com/matsievskiysv/linaccalc-equations}{\faGitlab\,/matsievskiysv/linaccalc-equations}
        \end{itemize}
\end{frame}

\begin{frame}{Contribution}
        \begin{itemize}
                \item Repeated derivation of known equations with two normalization variants
                \item Derived equations for transitional process calculation
                \item Tested code by comparing solution with 3D simulation
        \end{itemize}
\end{frame}

\begin{frame}{Features}
        \begin{itemize}
                \item Web UI
                \item Lazy evaluation
                \item Equation specification DSP language
                \item AST tree simplification
        \end{itemize}
\end{frame}

\begin{frame}{AST tree simplification}
        \centering{
                \resizebox{!}{0.8\textheight}{%
                        \input{images/ast-before.tikz}
                }
                \resizebox{!}{0.8\textheight}{%
                        \input{images/ast-after.tikz}
                }
        }
\end{frame}

\begin{frame}{Transitional process equations}
        \begin{equation*}
                \begin{bmatrix}
                        1       & k_{1;2}   & k_{1;3}
		        & \cdots
		        & k_{1;N-1} & k_{1;N} \\
                        k_{2;1} & 1         & k_{2;3}
		        & \cdots
		        & k_{2;N-1} & k_{2;N} \\
                        \cdots  & \cdots
		        & \cdots    & \ddots
		        & \cdots    & \cdots  \\
                        k_{N;1} & k_{N;2}   & k_{N;3}
		        & \cdots
		        & k_{N;N-1} & 1       \\
                \end{bmatrix}
                \begin{bmatrix}
                        \ddot x_1 \\ \ddot x_2 \\ \cdots \\ \ddot x_N \\
                \end{bmatrix}
                =
                \begin{bmatrix}
                        \operatorname{g}_1(\dot x, x, t) \\ \operatorname{g}_2(\dot x, x, t) \\
                        \cdots                           \\ \operatorname{g}_N(\dot x, x, t) \\
                \end{bmatrix}
        \end{equation*}

        \begin{multline*}
                \sum_{n=1}^{N} \left(
                        \frac{K^H_n}{2} \frac{f_0}{f_n} \frac{d^2x_n}{dt^2} -
                        2 \pi^2 K^E_n f_0^2 x_n
                \right) +
                \frac{d^2x}{dt^2} =
                -\frac{2 \pi  f_0}{Q_0} (1 + \chi_{\textup{г}} + \chi_{\textup{н}}) \frac{dx}{dt} - \\ -
                4 \pi^2 f_0^2 x +
                8 \pi f_0 f_{\textup{г}} \sqrt \frac{\pi f_0 P_{\textup{г}} \chi_{\textup{г}}}{Q_0}
                \cos{\left(2 \pi f_{\textup{г}} t + \varphi_0 \right)}
        \end{multline*}
\end{frame}

\section{Low level projects}

\subsection{SDM}

\begin{frame}{LinacCalc overview}
        \begin{itemize}
                \item Driver model for memory constrained IOT chip ESP8266 (ESP32)
                \item Core library written in C with API exposed to Lua
                \item Uniform driver access API
                \item Autogenerated Web UI
                \item \href{https://habr.com/post/449992/}{habr.com/post/449992/}
        \end{itemize}
\end{frame}

\begin{frame}{Features}
        \begin{itemize}
                \item Reminiscent of Linux driver model
                \item Devices and busses implemented using linked lists
        \end{itemize}
\end{frame}

\begin{frame}{Web UI}
        \includegraphics[height=0.43\textheight]{sdm_ui1}
        \hfill
        \includegraphics[height=0.43\textheight]{sdm_ui2}
\end{frame}

\subsection{Overseer}

\begin{frame}{Overseer overview}
        \begin{itemize}
                \item Message passing server
                \item Written in Rust with MySQL database
        \end{itemize}
\end{frame}

\begin{frame}{Features}
        \begin{itemize}
                \item Separate writer and reader threads
                \item Streamed processing using R based companion program
                \item Deployment using Docker
        \end{itemize}
\end{frame}

\section{Others}

\subsection{BeadPull}

\begin{frame}{BeadPull overview}
        \begin{itemize}
                \item PyQt interface to field distribution measurement bench
                \item Written in Python
                \item Hardware specific code for vector analyzer (GPIB bus) and motor encapsulated in <<drivers>>
        \end{itemize}
\end{frame}

\subsection{LegereLibrum}

\begin{frame}{LegereLibrum overview}
        \begin{itemize}
                \item Android View class for gridded bitmap (book pages) view
        \end{itemize}
\end{frame}

\begin{frame}{Challenges}
        \begin{itemize}
                \item Asynchronous coroutine based job queue with cancelable jobs
                \item Queues and caches may be dropped
                \item Maintaining <<page>> and <<screen>> coordinate system map
                \item Currently showing page tiles hot function
                \item Garbage collection
                \item Android API
        \end{itemize}
\end{frame}

\subsection{math-preview}

\begin{frame}{math-preview}
        \begin{itemize}
                \item Preview \LaTeX and MathML equations inline
        \end{itemize}
\end{frame}

\end{document}
